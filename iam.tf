
# Fargate task IAM role
data "aws_iam_policy_document" "fargate_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["ecs-tasks.amazonaws.com", "ecs.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "fargate_role" {
  name = "${var.component_name}-fargate"
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.fargate_role.json
}

resource "aws_efs_file_system_policy" "policy" {
  file_system_id = aws_efs_file_system.jenkins_data.id

  bypass_policy_lockout_safety_check = true
  policy                             = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "AccessThroughMountTarget",
    "Statement": [
        {
            "Sid": "AccessThroughMountTarget",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                 "elasticfilesystem:ClientRootAccess",
                "elasticfilesystem:ClientWrite",
                "elasticfilesystem:ClientMount"
            ],
            "Resource": "${aws_efs_mount_target.jenkins_data_mount_targets[0].file_system_arn}",
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "true"
                },
                "Bool": {
                    "elasticfilesystem:AccessedViaMountTarget": "true"
                }
            }
        },
        {
            "Sid": "FargateAccess",
            "Effect": "Allow",
            "Principal": { "AWS": "*" },
            "Action": [
                "elasticfilesystem:ClientMount",
                "elasticfilesystem:ClientWrite"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "true"
                },
                "StringEquals": {
                    "elasticfilesystem:AccessPointArn" : "${aws_efs_access_point.fargate.arn}"
                }
            }
        }
      
    ]
}
POLICY
  lifecycle {
    ignore_changes = [
      policy,
    ]
  }
}


data "aws_iam_policy_document" "fargate-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "fargate_task" {
  name   = "${var.component_name}-fargate-task-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ],
        "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "fargate_task" {
  role       = aws_iam_role.fargate_role.name
  policy_arn = aws_iam_policy.fargate_task.arn
}

resource "aws_efs_access_point" "fargate" {
  file_system_id = aws_efs_file_system.jenkins_data.id
  posix_user {
    gid = 1000
    uid = 1000
  }
  root_directory {
    path = "/opt/jenkins_data"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = 755
    }
  }
}
