# jenkins-image-release-tf

Using terraform to pull jenkins image in ecr and run as container in aws ecs

This module builds the infrastructure for **jenkins-image-release-tf** agen in the **BUILD-BACKEND** cell.

This module was built using [jenkins-image-release-tf](git@github.com:Bkoji1150/Jenkins-SonarQube-Terraform.git).

## Usage 
```hcl
module "jenkins_microservice" {
  source            = "git::git@gitlab.com:kojibello/ecs-task-definition-tf.git?ref=v1.0.0"

  component_name    = var.component_name
  cluster_name      = aws_ecs_cluster.ecs-jenkins.arn
  container_name    = var.container_name
  container_version = var.container_version
  container_port    = var.container_port
  target_group_health_check_path = "/login"

  certificate_arn        = module.acm.acm_certificate_arn
  ecs_service_subnet_ids = local.private_subnets
  vpc_id                 = local.vpc_id
  public_subnets         = local.public_subnet
  ecs_service_name = lower(format("%s-%s", var.cell_name, var.component_name))

  ecs_container_port = {
    jenkins_master = {
      from_port = var.container_port
      to_port   = var.container_port
    }
    jenkins_agent = {
      from_port = var.worker_port
      to_port   = var.worker_port
    }
  }
  container_port_mappings = [
    {
      containerPort = var.container_port
      hostPort      = var.container_port
      protocol      = "tcp"
    },
    {
      containerPort = var.worker_port
      hostPort      = var.worker_port
      protocol      = "tcp"
    }
  ]
  container_log_configuration = {
    logDriver = "awslogs"
    options = {
      "awslogs-create-group"  = "true"
      "awslogs-group"         = aws_cloudwatch_log_group.awslogs-group.name
      "awslogs-region"        = var.aws_region
      "awslogs-stream-prefix" = var.container_name
    }
    secretOptions = null
  }
  volumes = [{
    name                        = "${var.component_name}-jenkins-efs"
    host_path                   = null
    docker_volume_configuration = []
    efs_volume_configuration = [{
      file_system_id          = aws_efs_file_system.jenkins_data.id
      root_directory          = "/"
      transit_encryption      = "ENABLED"
      transit_encryption_port = null
      authorization_config = [{
        access_point_id = aws_efs_access_point.fargate.id
        iam             = "ENABLED"
      }]
    }]
  }]
  container_mount_points = [
    {
      sourceVolume  = "${var.component_name}-jenkins-efs"
      containerPath = var.target_group_health_check_path
      readOnly      = false
    }
  ]
}

```

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.3.1 |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.3.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.0 ~> 4.0 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_acm"></a> [acm](#module\_acm) | terraform-aws-modules/acm/aws | 3.0.0 |
| <a name="module_jenkins_microservice"></a> [jenkins\_microservice](#module\_jenkins\_microservice) | git::git@gitlab.com:kojibello/ecs-task-definition-tf.git | v1.0.0 |
| <a name="module_required_tags"></a> [required\_tags](#module\_required\_tags) | git::https://github.com/Bkoji1150/kojitechs-tf-aws-required-tags.git | v1.0.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.awslogs-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_cluster.ecs-jenkins](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster) | resource |
| [aws_efs_access_point.fargate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_access_point) | resource |
| [aws_efs_file_system.jenkins_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system) | resource |
| [aws_efs_file_system_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system_policy) | resource |
| [aws_efs_mount_target.jenkins_data_mount_targets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_mount_target) | resource |
| [aws_iam_policy.fargate_task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.fargate_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.fargate_task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_route53_record.dns_record](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_security_group.jenkins_data_allow_nfs_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.jenkins_data_allow_nfs_access_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_iam_policy_document.fargate-role-policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.fargate_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_route53_zone.zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [terraform_remote_state.operational_environment](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ado"></a> [ado](#input\_ado) | HIDS ADO that owns the resource. The ServiceNow Contracts table is the system of record for the actual ADO names and LOB names. | `string` | `"Kojitechs"` | no |
| <a name="input_application"></a> [application](#input\_application) | Logical name for the application. Mainly used for kojitechs. For an ADO/LOB owned application default to the LOB name. | `string` | `"jenkins_build_agent"` | no |
| <a name="input_application_owner"></a> [application\_owner](#input\_application\_owner) | Email Address of the group who owns the application. This should be a distribution list and no an individual email if at all possible. Primarily used for Ventech-owned applications to indicate what group/department is responsible for the application using this resource. For an ADO/LOB owned application default to the LOB name. | `string` | `"kojitechs@gmail.com"` | no |
| <a name="input_aws_account_id"></a> [aws\_account\_id](#input\_aws\_account\_id) | Environment this template would be deployed to | `map(string)` | `{}` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region. | `string` | `"us-east-1"` | no |
| <a name="input_builder"></a> [builder](#input\_builder) | The name of the person who created the resource. | `string` | `"kojitechs@gmail.com"` | no |
| <a name="input_cell_name"></a> [cell\_name](#input\_cell\_name) | The name of the cell. | `string` | `"KOJITECHS"` | no |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Name of the ECS cluster to deploy the service into. | `string` | `"test"` | no |
| <a name="input_component_name"></a> [component\_name](#input\_component\_name) | Name of the component. | `string` | `"buildagent"` | no |
| <a name="input_container_name"></a> [container\_name](#input\_container\_name) | container name | `string` | `"jenkins-master-node-release"` | no |
| <a name="input_container_port"></a> [container\_port](#input\_container\_port) | Port that this service will listen on. | `number` | `8080` | no |
| <a name="input_container_version"></a> [container\_version](#input\_container\_version) | The image used to start a container. Up to 255 letters (uppercase and lowercase), numbers, hyphens, underscores, colons, periods, forward slashes, and number signs are allowed. | `string` | `"2.2.0"` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | Domain name | `any` | n/a | yes |
| <a name="input_ecs_service_desired_count"></a> [ecs\_service\_desired\_count](#input\_ecs\_service\_desired\_count) | Number of tasks to launch in the ECS service. | `number` | `1` | no |
| <a name="input_line_of_business"></a> [line\_of\_business](#input\_line\_of\_business) | HIDS LOB that owns the resource. | `string` | `"TECH"` | no |
| <a name="input_subject_alternative_names"></a> [subject\_alternative\_names](#input\_subject\_alternative\_names) | n/a | `list(any)` | `[]` | no |
| <a name="input_target_group_health_check_path"></a> [target\_group\_health\_check\_path](#input\_target\_group\_health\_check\_path) | Path that will be used to perform the health check of the ECS service. | `string` | `"/var/jenkins_home"` | no |
| <a name="input_tech_poc_primary"></a> [tech\_poc\_primary](#input\_tech\_poc\_primary) | Email Address of the Primary Technical Contact for the AWS resource. | `string` | `"kojitechs@gmail.com"` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | Network tier or layer where the resource resides. These tiers are represented in every VPC regardless of single-tenant or multi-tenant. For most resources in the Infrastructure and Security VPC, the TIER will be Management. But in some cases,such as Atlassian, the other tiers are relevant. | `string` | `"APP"` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The VPC the resource resides in. We need this to differentiate from Lifecycle Environment due to INFRA and SEC. One of "APP", "INFRA", "SEC", "ROUTING". | `string` | `"APP"` | no |
| <a name="input_worker_port"></a> [worker\_port](#input\_worker\_port) | Port that this service will listen on. | `number` | `50000` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alb_hostname"></a> [alb\_hostname](#output\_alb\_hostname) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Authors

Module is maintained by [kOJI BELLO](https://github.com/antonbabenko) with help from [these awesome contributors](https://github.com/terraform-aws-modules/terraform-aws-autoscaling/graphs/contributors).
